#define TWI_BUFFER_LENGTH 48

//Defines Actual 328p Pins to Arduino numbers
#define PC0 A0
#define PC1 A1
#define PC2 A2
#define PC3 A3
#define PD0 0
#define PD1 1
#define PD2 2
#define PD3 3 //pwm
#define PD4 4
#define PD5 5 //pwm
#define PD6 6 //pwm
#define PD7 7
#define PB0 8
#define PB1 9 //pwm
#define PB2 10
#define ADC6 A6
#define ADC7 A7

//Defines More descriptive names to 328p Pins.
#define FCUR1 PC0
#define FCUR2 PC1
#define FCUR3 PC2
#define FCUR4 PC3
#define FANSUPVOLT  ADC6
#define ONBOARDTEMP ADC7
#define FSEN1 PD0
#define FSEN2 PD1
#define FSEN3 PD2
#define FSEN4 PD4
#define FPWM1 PD3
#define FPWM2 PD5
#define FPWM3 PD6
#define FPWM4 PB1
#define WDI     PD7
#define FANON   PB0
#define ONEWIRE PB2

#define FCURDIR1 INPUT
#define FCURDIR2 INPUT
#define FCURDIR3 INPUT
#define FCURDIR4 INPUT
#define FANSUPVOLT_DIR  INPUT
#define ONBOARDTEMP_DIR INPUT
#define FSENDIR1 INPUT_PULLUP
#define FSENDIR2 INPUT_PULLUP
#define FSENDIR3 INPUT_PULLUP
#define FSENDIR4 INPUT_PULLUP
#define FPWMDIR1 OUTPUT
#define FPWMDIR2 OUTPUT
#define FPWMDIR3 OUTPUT
#define FPWMDIR4 OUTPUT
#define WDI_DIR     OUTPUT
#define FANON_DIR   OUTPUT
#define ONEWIRE_DIR OUTPUT

//Default defines
#define ITEMPOFFSET 1.65
#define ETEMPOFFSET 1
#define SUPPLYTOPR 20000.0
#define SUPPLYBOTR 4700.0

//EEprom memory map defines
#define EECRC      0
#define EEADDR     1
#define EEFANEN    2
#define EEFANPWM1  3
#define EEFANPWM2  4
#define EEFANPWM3  5
#define EEFANPWM4  6
#define EETEMPMIN1 7
#define EETEMPMIN2 8
#define EETEMPMIN3 9
#define EETEMPMIN4 10
#define EETEMPMAX1 11
#define EETEMPMAX2 12
#define EETEMPMAX3 13
#define EETEMPMAX4 14
#define EEFUZZBAND 15
#define EESTEP	   16
#define EEFPWMMIN1 17
#define EEFPWMMIN2 18
#define EEFPWMMIN3 19
#define EEFPWMMIN4 20
#define EEFPWMMAX1 21
#define EEFPWMMAX2 22
#define EEFPWMMAX3 23
#define EEFPWMMAX4 24
#define EEPOLLTIME 25//4bytes
#define EECURPTIME 29//4bytes
#define EEMAXCUR1  33//2bytes
#define EEMAXCUR2  35//2bytes
#define EEMAXCUR3  37//2bytes
#define EEMAXCUR4  39//2bytes
#define EEMINRPM1  41//2bytes
#define EEMINRPM2  43//2bytes
#define EEMINRPM3  45//2bytes
#define EEMINRPM4  47//2bytes
#define EEMAXRPM1  49//2bytes
#define EEMAXRPM2  51//2bytes
#define EEMAXRPM3  53//2bytes
#define EEMAXRPM4  55//2bytes
#define EERPMBAND  57//2bytes
#define EECURBAND  59//2bytes
#define EEFAULTON  61

//I2C Command list
#define CMDFULLSTATUS   1  //Returns a full status of Controller
#define CMDQUICKSTATUS  2  //Returns a quick status
#define CMDFANSTATUS1	3  //Current status of Fan 1
#define CMDFANSTATUS2	4  //Current status of Fan 2
#define CMDFANSTATUS3	5  //Current status of Fan 3
#define CMDFANSTATUS4	6  //Current status of Fan 4
#define CMDENABLE		7  //Enable all fans
#define CMDDISABLE		8  //Disable all fans
#define CMDENABLEFAN1	9  //Enable fan 1
#define CMDENABLEFAN2	10 //Enable fan 2
#define CMDENABLEFAN3	11 //Enable fan 3
#define CMDENABLEFAN4	12 //Enable fan 4
#define CMDDISABLEFAN1	13 //Disable fan 1
#define CMDDISABLEFAN2	14 //Disable fan 2
#define CMDDISABLEFAN3	15 //Disable fan 3
#define CMDDISABLEFAN4	16 //Disable fan 4
#define CMDSETALLON		17 //Sets Duty on all fans to 100%
#define CMDSETALLOFF	18 //Sets Duty on all fans to 0%
#define CMDSETDUTY1		19 //Sets Duty on fan 1
#define CMDSETDUTY2		20 //Sets Duty on fan 1
#define CMDSETDUTY3		21 //Sets Duty on fan 1
#define CMDSETDUTY4		22 //Sets Duty on fan 1
#define CMDSETMINTEMP	23 //Sets the Off Temperature for all fans
#define CMDSETMAXTEMP	24 //Sets the Max Duty Temperature for all fans
#define CMDSETFUZZY		25 //Sets the Switchover band to stop pulsing
#define CMDSETSTEPS		26 //Sets the number of duty steps between off and max (0 - Max temp fan comes on, Off temp fan switched off)
#define CMDSETMINTEMP1  27 //Sets the Off Temperature for fan 1
#define CMDSETMINTEMP2  28 //Sets the Off Temperature for fan 2
#define CMDSETMINTEMP3  29 //Sets the Off Temperature for fan 3
#define CMDSETMINTEMP4  30 //Sets the Off Temperature for fan 4
#define CMDSETMAXTEMP1	31 //Sets the Max Temperature for fan 1
#define CMDSETMAXTEMP2	32 //Sets the Max Temperature for fan 2
#define CMDSETMAXTEMP3	33 //Sets the Max Temperature for fan 3
#define CMDSETMAXTEMP4	34 //Sets the Max Temperature for fan 4
#define CMDLEARNFAN		35 //Learns the fans max speed and current usage.
#define CMDSETADDRESS	36 //Defines the I2C address to use (44 is the default address) Must be followed with 83 101 116 (Set).
#define CMDFACTORYRESET 37 //Clears the EEPROM Must be followed with 89 101 115 (Yes)
#define CMDCOMITCHANGES 38 //Commits the current settings to EEPROM. Must be followed with 83 101 116 (Set)
#define CMDHARDRESET	39 //Performs a hard reset to reload from EEPROM. Must have WDT installed.
#define CMDSETMINDUTY1	40 //Sets the Minimum duty allowed for fan 1
#define CMDSETMINDUTY2	41 //Sets the Minimum duty allowed for fan 2
#define CMDSETMINDUTY3	42 //Sets the Minimum duty allowed for fan 3
#define CMDSETMINDUTY4	43 //Sets the Minimum duty allowed for fan 4
#define CMDSETMAXDUTY1	44 //Sets the Maximum duty allowed for fan 1
#define CMDSETMAXDUTY2	45 //Sets the Maximum duty allowed for fan 2
#define CMDSETMAXDUTY3	46 //Sets the Maximum duty allowed for fan 3
#define CMDSETMAXDUTY4	47 //Sets the Maximum duty allowed for fan 4
#define CMDSETAUTO1		48 //Sets a fan to be temperature controlled, switching off the override.
#define CMDSETAUTO2		49 //Sets a fan to be temperature controlled, switching off the override.
#define CMDSETAUTO3		50 //Sets a fan to be temperature controlled, switching off the override.
#define CMDSETAUTO4		51 //Sets a fan to be temperature controlled, switching off the override.
#define CMDSETRPMBAND   52 //Sets the Window that the RPM is allowed to be in.
#define CMDSETCURBAND	53 //Sets the Window that the Current is allowed to be in.
#define CMDSETFAULTON   54 //Sets FANON pin to indicate if there is a fan fault
#define CMDSETFAULTOFF  55 //Sets FANON pin to indicate that fans are on or off
#define CMDENABLESERIAL 56 //Enables Serial Debugging.

//Status Message format
//Complete message all the values below. Quick Status first section only. Fan status second section only -10 subtracted from address
//00   - internal temp
//01   - external temp
//02   - overall current high
//03   - overall current low
//04   - fault
//05   - fan Enabled And On (top 4 bits enabled, bottom 4 bits On status)
//06   - Current Steps
//07   - Fuzzy Band
//08   - supply Voltage (high 4bits, decimal value, low 4 bits whole value)
//09   - Cpu Voltage (high 4bits, decimal value, low 4 bits whole value)
//FAN 1
//10   - fanRPM High
//11   - fanPRM Low
//12   - fanCurrent High
//13   - fanCurrent Low
//14   - current Duty
//15.7 - enabled
//15.6 - override
//15.5 - fault
//16   - mintemp
//17   - maxtemp
//FAN 2
//20   - fanRPM High
//21   - fanPRM Low
//22   - fanCurrent High
//23   - fanCurrent Low
//24   - current Duty
//25.7 - enabled
//25.6 - override
//25.5 - fault
//26   - mintemp
//27   - maxtemp
//FAN 3
//30   - fanRPM High
//31   - fanPRM Low
//32   - fanCurrent High
//33   - fanCurrent Low
//34   - current Duty
//35.7 - enabled
//35.6 - override
//35.5 - fault
//36   - mintemp
//37   - maxtemp
//FAN 4
//40   - fanRPM High
//41   - fanPRM Low
//42   - fanCurrent High
//43   - fanCurrent Low
//44   - current Duty
//45.7 - enabled
//45.6 - override
//45.5 - fault
//46   - mintemp
//47   - maxtemp

#define SOFTDEBUGENABLED

#ifdef DEBUGENABLED
#define DEBUG(x) Serial.print(x)
#define DEBUGLN(x) Serial.println(x)
#define DEBUGF(x) Serial.printf(x)
#define SETUPSERIAL Serial.begin(19200)
#define VERBOSEOUT
#else
#ifdef SOFTDEBUGENABLED
#define DEBUG(x) if (serialenabled) mySerial.print(x)
#define DEBUGLN(x) if (serialenabled) mySerial.println(x)
#define DEBUGF(x) if (serialenabled) mySerial.printf(x)
#define SETUPSERIAL if (serialenabled) mySerial.begin(19200)
#define VERBOSEOUT
#else
#define SETUPSERIAL
#define DEBUG(x)
#define DEBUGLN(x)
#define DEBUGF(x)
#endif
#endif

#include <OneWire.h>
#include <DallasTemperature.h>
#include <Wire.h>
#include <util/crc16.h>
#include <EEPROM.h>

#ifdef SOFTDEBUGENABLED
#include <SoftwareSerial.h>
SoftwareSerial mySerial(FPWM3,FANON);
#endif

OneWire oneWire(ONEWIRE);
DallasTemperature onewireTemp(&oneWire);
unsigned long conversionWaitTime = 0;
unsigned long curConvertTime = 0;
unsigned long currentConversionPeriod,conversionPollPeriod = 0;
float oneWireTempInC = 0.0;

float internalTemp = 0.0;
float supplyVoltage = 0.0;
float cpuVoltage = 0.0;
unsigned long fanCalcTemp;
uint8_t i2cAddress = 0;
uint8_t buf[43];
boolean factoryReset = false;
boolean learnFans = false;
boolean pollWDT = true;
boolean noOneWire = false;
boolean raiseAlarms = false;
boolean systemFault=false;
boolean fansOn=false;
boolean serialenabled=false;
int fanSteps = 255;
int fuzzyBand = 2;
int rpmBand = 1000;
int curBand = 100;
uint8_t i2cBuffer[16];
uint8_t i2cDataLen;
unsigned long cur_fanAvg[50];
unsigned long cur_total;
uint8_t cur_index,cur_filled=0;
volatile uint8_t status=0; 

struct fan_type {
	byte pwmpinnum;
	byte curpinnum;
	byte senpinnum;
	byte minpwm;
	byte maxpwm;
	byte pwmval;
	int mintemp;
	int maxtemp;
	unsigned int fanCnt;
	unsigned int fanAvgCnt;
	unsigned int retry;
	unsigned long fantoggleTime;
	unsigned long fanRPM;
	unsigned long minRPM;
	unsigned long maxRPM;
	unsigned long maxCurrent;
	unsigned long fanCur;
	unsigned long timestamp;
	uint8_t faultreason;
	boolean enabled;
	boolean fault;
	boolean override;
	boolean fantoggle;
	
};

fan_type fans[4]={
{FPWM1,FCUR1,FSEN1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,true,false,false,false},
{FPWM2,FCUR2,FSEN2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,true,false,false,false},
{FPWM3,FCUR3,FSEN3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,true,false,false,false},
{FPWM4,FCUR4,FSEN4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,true,false,false,false}};

boolean wdtToggle=false;

unsigned long serialTime = 0;
unsigned int loopcount=0;

void setup() {
	
	SETUPSERIAL;
	
	i2cAddress = 44;
	
	DEBUGLN("Fan Controller - Peguin V1.0.0");
	
	readEeprom();
	
	onewireTemp.begin();
	onewireTemp.setWaitForConversion(false);
	onewireTemp.requestTemperatures();
	conversionWaitTime = millis();
	serialTime = millis();
	Wire.begin(i2cAddress);
	Wire.onReceive(processI2CEvent);
	Wire.onRequest(sendI2CStatus);
	

	pinMode(FCUR1       ,FCURDIR1);
	pinMode(FCUR2       ,FCURDIR2);
	pinMode(FCUR3       ,FCURDIR3);
	pinMode(FCUR4       ,FCURDIR4);
	pinMode(FANSUPVOLT  ,FANSUPVOLT_DIR);
	pinMode(ONBOARDTEMP ,ONBOARDTEMP_DIR);
	pinMode(FSEN1       ,FSENDIR1);
	pinMode(FSEN2       ,FSENDIR2);
	pinMode(FSEN3       ,FSENDIR3);
	pinMode(FSEN4       ,FSENDIR4);
	pinMode(FPWM1       ,FPWMDIR1);
	pinMode(FPWM2       ,FPWMDIR2);
	pinMode(FPWM3       ,FPWMDIR3);
	pinMode(FPWM4       ,FPWMDIR4);
	pinMode(WDI         ,WDI_DIR);
	pinMode(FANON      ,FANON_DIR);
	
	
	for (int a=0;a<4;a++){
		fans[a].fantoggleTime=millis();
		fans[a].pwmval=255;
		if (fans[a].enabled) analogWrite(fans[a].pwmpinnum,fans[a].pwmval);
	}
	
	resetCur();

	readVcc();
	
	DEBUGLN("System Ready");
	
}

void storeEeprom() {
	DEBUGLN("Writting EEPROM");
	uint8_t crc=0;
	EEPROM.write(EEADDR,i2cAddress);
	crc=_crc_ibutton_update(crc,(uint8_t)i2cAddress);
	uint8_t fanEnableAcitveBits=0;
	for(int a=0;a<4;a++){
		fanEnableAcitveBits|=fans[a].enabled<<a;
		fanEnableAcitveBits|=fans[a].override<<(a+4);
	}
	EEPROM.write(EEFANEN,fanEnableAcitveBits);
	crc=_crc_ibutton_update(crc,fanEnableAcitveBits);
	EEPROM.write(EEFANPWM1,fans[0].pwmval);
	crc=_crc_ibutton_update(crc,fans[0].pwmval);
	EEPROM.write(EEFANPWM2,fans[1].pwmval);
	crc=_crc_ibutton_update(crc,fans[1].pwmval);
	EEPROM.write(EEFANPWM3,fans[2].pwmval);
	crc=_crc_ibutton_update(crc,fans[2].pwmval);
	EEPROM.write(EEFANPWM4,fans[3].pwmval);
	crc=_crc_ibutton_update(crc,fans[3].pwmval);
	EEPROM.write(EETEMPMIN1,fans[0].mintemp);
	crc=_crc_ibutton_update(crc,fans[0].mintemp);
	EEPROM.write(EETEMPMIN2,fans[1].mintemp);
	crc=_crc_ibutton_update(crc,fans[1].mintemp);
	EEPROM.write(EETEMPMIN3,fans[2].mintemp);
	crc=_crc_ibutton_update(crc,fans[2].mintemp);
	EEPROM.write(EETEMPMIN4,fans[3].mintemp);
	crc=_crc_ibutton_update(crc,fans[3].mintemp);
	EEPROM.write(EETEMPMAX1,fans[0].maxtemp);
	crc=_crc_ibutton_update(crc,fans[0].maxtemp);
	EEPROM.write(EETEMPMAX2,fans[1].maxtemp);
	crc=_crc_ibutton_update(crc,fans[1].maxtemp);
	EEPROM.write(EETEMPMAX3,fans[2].maxtemp);
	crc=_crc_ibutton_update(crc,fans[2].maxtemp);
	EEPROM.write(EETEMPMAX4,fans[3].maxtemp);
	crc=_crc_ibutton_update(crc,fans[3].maxtemp);
	EEPROM.write(EEFUZZBAND,fuzzyBand);
	crc=_crc_ibutton_update(crc,fuzzyBand);
	EEPROM.write(EESTEP,fanSteps);
	crc=_crc_ibutton_update(crc,fanSteps);
	EEPROM.write(EEFPWMMIN1,fans[0].minpwm);
	crc=_crc_ibutton_update(crc,fans[0].minpwm);
	EEPROM.write(EEFPWMMAX1,fans[0].maxpwm);
	crc=_crc_ibutton_update(crc,fans[0].maxpwm);
	EEPROM.write(EEFPWMMIN2,fans[1].minpwm);
	crc=_crc_ibutton_update(crc,fans[1].minpwm);
	EEPROM.write(EEFPWMMAX2,fans[1].maxpwm);
	crc=_crc_ibutton_update(crc,fans[1].maxpwm);
	EEPROM.write(EEFPWMMIN3,fans[2].minpwm);
	crc=_crc_ibutton_update(crc,fans[2].minpwm);
	EEPROM.write(EEFPWMMAX3,fans[2].maxpwm);
	crc=_crc_ibutton_update(crc,fans[2].maxpwm);
	EEPROM.write(EEFPWMMIN4,fans[3].minpwm);
	crc=_crc_ibutton_update(crc,fans[3].minpwm);
	EEPROM.write(EEFPWMMAX4,fans[3].maxpwm);
	crc=_crc_ibutton_update(crc,fans[3].maxpwm);
	EEPROM.write(EECURPTIME,(uint8_t)currentConversionPeriod);
	crc=_crc_ibutton_update(crc,(uint8_t)currentConversionPeriod);
	EEPROM.write(EECURPTIME+1,(uint8_t)(currentConversionPeriod>>8));
	crc=_crc_ibutton_update(crc,(uint8_t)(currentConversionPeriod>>8));
	EEPROM.write(EECURPTIME+2,(uint8_t)(currentConversionPeriod>>16));
	crc=_crc_ibutton_update(crc,(uint8_t)(currentConversionPeriod>>16));
	EEPROM.write(EECURPTIME+3,(uint8_t)(currentConversionPeriod>>24));
	crc=_crc_ibutton_update(crc,(uint8_t)(currentConversionPeriod>>24));
	EEPROM.write(EEPOLLTIME,(uint8_t)conversionPollPeriod);
	crc=_crc_ibutton_update(crc,(uint8_t)conversionPollPeriod);
	EEPROM.write(EEPOLLTIME+1,(uint8_t)(conversionPollPeriod>>8));
	crc=_crc_ibutton_update(crc,(uint8_t)(conversionPollPeriod>>8));
	EEPROM.write(EEPOLLTIME+2,(uint8_t)(conversionPollPeriod>>16));
	crc=_crc_ibutton_update(crc,(uint8_t)(conversionPollPeriod>>16));
	EEPROM.write(EEPOLLTIME+3,(uint8_t)(conversionPollPeriod>>24));
	crc=_crc_ibutton_update(crc,(uint8_t)(conversionPollPeriod>>24));
	
	for (int a=0;a<4;a++) {
		EEPROM.write(EEMAXCUR1+(2*a),(uint8_t)fans[a].maxCurrent);
		crc=_crc_ibutton_update(crc,(uint8_t)fans[a].maxCurrent);
		EEPROM.write((EEMAXCUR1+(2*a))+1,(uint8_t)(fans[a].maxCurrent>>8));
		crc=_crc_ibutton_update(crc,(uint8_t)(fans[a].maxCurrent>>8));
		
		EEPROM.write(EEMINRPM1+(2*a),(uint8_t)fans[a].minRPM);
		crc=_crc_ibutton_update(crc,(uint8_t)fans[a].minRPM);
		EEPROM.write((EEMINRPM1+(2*a))+1,(uint8_t)(fans[a].minRPM>>8));
		crc=_crc_ibutton_update(crc,(uint8_t)(fans[a].minRPM>>8));
		
		EEPROM.write(EEMAXRPM1+(2*a),(uint8_t)fans[a].maxRPM);
		crc=_crc_ibutton_update(crc,(uint8_t)fans[a].maxRPM);
		EEPROM.write((EEMAXRPM1+(2*a))+1,(uint8_t)(fans[a].maxRPM>>8));
		crc=_crc_ibutton_update(crc,(uint8_t)(fans[a].maxRPM>>8));
		
	}
	
	EEPROM.write(EERPMBAND,(uint8_t)rpmBand);
	crc=_crc_ibutton_update(crc,(uint8_t)rpmBand);
	EEPROM.write(EERPMBAND+1,(uint8_t)(rpmBand>>8));
	crc=_crc_ibutton_update(crc,(uint8_t)(rpmBand>>8));
	
	EEPROM.write(EECURBAND,(uint8_t)curBand);
	crc=_crc_ibutton_update(crc,(uint8_t)curBand);
	EEPROM.write(EECURBAND+1,(uint8_t)(curBand>>8));
	crc=_crc_ibutton_update(crc,(uint8_t)(curBand>>8));
	
	EEPROM.write(EEFAULTON,raiseAlarms);
	crc=_crc_ibutton_update(crc,raiseAlarms);
	
	EEPROM.write(EECRC,crc);
	DEBUG("Storing CRC :");
	DEBUGLN(crc);
}

void readEeprom() {
	DEBUGLN("Reading EEPROM");
	uint8_t crc=0;
	uint8_t interim=0;
	uint8_t eecrc=0;
	eecrc=EEPROM.read(EECRC);
	i2cAddress=EEPROM.read(EEADDR);
	crc=_crc_ibutton_update(crc,i2cAddress);
	uint8_t fanEnableAcitveBits=EEPROM.read(EEFANEN);
	for(int a=0;a<4;a++){
		fans[a].enabled=(fanEnableAcitveBits>>a)&1;
		fans[a].override=(fanEnableAcitveBits>>(a+4))&1;
	}
	crc=_crc_ibutton_update(crc,fanEnableAcitveBits);
	fans[0].pwmval=EEPROM.read(EEFANPWM1);
	crc=_crc_ibutton_update(crc,fans[0].pwmval);
	fans[1].pwmval=EEPROM.read(EEFANPWM2);
	crc=_crc_ibutton_update(crc,fans[1].pwmval);
	fans[2].pwmval=EEPROM.read(EEFANPWM3);
	crc=_crc_ibutton_update(crc,fans[2].pwmval);
	fans[3].pwmval=EEPROM.read(EEFANPWM4);
	crc=_crc_ibutton_update(crc,fans[3].pwmval);
	fans[0].mintemp=EEPROM.read(EETEMPMIN1);
	crc=_crc_ibutton_update(crc,fans[0].mintemp);
	fans[1].mintemp=EEPROM.read(EETEMPMIN2);
	crc=_crc_ibutton_update(crc,fans[1].mintemp);
	fans[2].mintemp=EEPROM.read(EETEMPMIN3);
	crc=_crc_ibutton_update(crc,fans[2].mintemp);
	fans[3].mintemp=EEPROM.read(EETEMPMIN4);
	crc=_crc_ibutton_update(crc,fans[3].mintemp);
	fans[0].maxtemp=EEPROM.read(EETEMPMAX1);
	crc=_crc_ibutton_update(crc,fans[0].maxtemp);
	fans[1].maxtemp=EEPROM.read(EETEMPMAX2);
	crc=_crc_ibutton_update(crc,fans[1].maxtemp);
	fans[2].maxtemp=EEPROM.read(EETEMPMAX3);
	crc=_crc_ibutton_update(crc,fans[2].maxtemp);
	fans[3].maxtemp=EEPROM.read(EETEMPMAX4);
	crc=_crc_ibutton_update(crc,fans[3].maxtemp);
	fuzzyBand=EEPROM.read(EEFUZZBAND);
	crc=_crc_ibutton_update(crc,fuzzyBand);
	fanSteps=EEPROM.read(EESTEP);
	crc=_crc_ibutton_update(crc,fanSteps);
	fans[0].minpwm=EEPROM.read(EEFPWMMIN1);
	crc=_crc_ibutton_update(crc,fans[0].minpwm);
	fans[0].maxpwm=EEPROM.read(EEFPWMMAX1);
	crc=_crc_ibutton_update(crc,fans[0].maxpwm);
	fans[1].minpwm=EEPROM.read(EEFPWMMIN2);
	crc=_crc_ibutton_update(crc,fans[1].minpwm);
	fans[1].maxpwm=EEPROM.read(EEFPWMMAX2);
	crc=_crc_ibutton_update(crc,fans[1].maxpwm);
	fans[2].minpwm=EEPROM.read(EEFPWMMIN3);
	crc=_crc_ibutton_update(crc,fans[2].minpwm);
	fans[2].maxpwm=EEPROM.read(EEFPWMMAX3);
	crc=_crc_ibutton_update(crc,fans[2].maxpwm);
	fans[3].minpwm=EEPROM.read(EEFPWMMIN4);
	crc=_crc_ibutton_update(crc,fans[3].minpwm);
	fans[3].maxpwm=EEPROM.read(EEFPWMMAX4);
	crc=_crc_ibutton_update(crc,fans[3].maxpwm);
	interim=EEPROM.read(EECURPTIME);
	crc=_crc_ibutton_update(crc,interim);
	currentConversionPeriod=interim;
	interim=EEPROM.read(EECURPTIME+1);
	crc=_crc_ibutton_update(crc,interim);
	currentConversionPeriod|=(unsigned long)interim<<8;
	interim=EEPROM.read(EECURPTIME+2);
	crc=_crc_ibutton_update(crc,interim);
	currentConversionPeriod|=(unsigned long)interim<<16;
	interim=EEPROM.read(EECURPTIME+3);
	crc=_crc_ibutton_update(crc,interim);
	currentConversionPeriod|=(unsigned long)interim<<24;
	interim=EEPROM.read(EEPOLLTIME);
	crc=_crc_ibutton_update(crc,interim);
	conversionPollPeriod=interim;
	interim=EEPROM.read(EEPOLLTIME+1);
	crc=_crc_ibutton_update(crc,interim);
	conversionPollPeriod|=(unsigned long)interim<<8;
	interim=EEPROM.read(EEPOLLTIME+2);
	crc=_crc_ibutton_update(crc,interim);
	conversionPollPeriod|=(unsigned long)interim<<16;
	interim=EEPROM.read(EEPOLLTIME+3);
	crc=_crc_ibutton_update(crc,interim);
	conversionPollPeriod|=(unsigned long)interim<<24;
	
	for (int a=0;a<4;a++) {
		interim=EEPROM.read(EEMAXCUR1+(2*a));
		crc=_crc_ibutton_update(crc,interim);
		fans[a].maxCurrent=interim;
		interim=EEPROM.read((EEMAXCUR1+(2*a))+1);
		crc=_crc_ibutton_update(crc,interim);
		fans[a].maxCurrent|=(unsigned long)interim<<8;
		
		interim=EEPROM.read(EEMINRPM1+(2*a));
		crc=_crc_ibutton_update(crc,interim);
		fans[a].minRPM=interim;
		interim=EEPROM.read((EEMINRPM1+(2*a))+1);
		crc=_crc_ibutton_update(crc,interim);
		fans[a].minRPM|=(unsigned long)interim<<8;
		
		interim=EEPROM.read(EEMAXRPM1+(2*a));
		crc=_crc_ibutton_update(crc,interim);
		fans[a].maxRPM=interim;
		interim=EEPROM.read((EEMAXRPM1+(2*a))+1);
		crc=_crc_ibutton_update(crc,interim);
		fans[a].maxRPM|=(unsigned long)interim<<8;
		
	}
	
	interim=EEPROM.read(EERPMBAND);
	crc=_crc_ibutton_update(crc,interim);
	rpmBand=interim;
	interim=EEPROM.read(EERPMBAND+1);
	crc=_crc_ibutton_update(crc,interim);
	rpmBand|=(unsigned long)interim<<8;
	
	interim=EEPROM.read(EECURBAND);
	crc=_crc_ibutton_update(crc,interim);
	curBand=interim;
	interim=EEPROM.read(EECURBAND+1);
	crc=_crc_ibutton_update(crc,interim);
	curBand|=(unsigned long)interim<<8;
	
	raiseAlarms=EEPROM.read(EEFAULTON);
	crc=_crc_ibutton_update(crc,raiseAlarms);

	
	DEBUG("Read CRC:");
	DEBUG(eecrc);
	DEBUG(" Calculated :");
	DEBUGLN(crc);
	if (crc!=eecrc) defaultEeprom();
	
}

void defaultEeprom() {
	DEBUGLN("Defaulting EEPROM");
	for(int a=0;a<4;a++){
		fans[a].enabled=true;
		fans[a].maxtemp=55;
		fans[a].pwmval=0;
		fans[a].override=false;
		fans[a].minpwm=255;
		fans[a].maxpwm=255;
	}
	fans[0].mintemp=30;
	fans[1].mintemp=35;
	fans[2].mintemp=40;
	fans[3].mintemp=45;
	fuzzyBand=1;
	fanSteps=255;
	rpmBand=1000;
	raiseAlarms=false;
	curBand=100;
	i2cAddress=44;
	currentConversionPeriod=500;
	conversionPollPeriod=200;
	
	storeEeprom();
}

void quickStatus(uint8_t *buf)
{
	uint16_t overAllCurrent=0;
	buf[4]=0;
	buf[5]=0;
	buf[6]=0;
	for(int a=0;a<4;a++){
		if (fans[a].enabled) {
			buf[6]|=1<<(a+4);
			if (fans[a].pwmval>0) buf[6]|=1<<a;
			overAllCurrent+=fans[a].fanCur;
		} 
		if (fans[a].fault) buf[5]|=1<<a;
		if (fans[a].override) buf[4]|=1<<a;
	}
	buf[7]=floor(supplyVoltage);
	buf[8]=(uint8_t)((supplyVoltage-(float)buf[7])*100);
	buf[9]=floor(cpuVoltage);
	buf[10]=(uint8_t)((cpuVoltage-(float)buf[9])*100);
	
	buf[0]=(uint8_t)round(internalTemp);
	buf[1]=(uint8_t)round(oneWireTempInC);
	buf[2]=(uint8_t)overAllCurrent;
	buf[3]=(uint8_t)(overAllCurrent<<8);
	
	DEBUG("[");
	
	for (int a=0;a<11;a++) {
		DEBUG(buf[a]);
		DEBUG(" ");
	}
	
	DEBUGLN("]");

}


void fanStatus(uint8_t *buf,uint8_t fan,boolean fs){
	
	uint8_t a;
	if (fs) a=0;
	else {
		a=11+fan*8;
	}
	
	buf[0+a]=(uint8_t)fans[fan].fanRPM;
	buf[1+a]=(uint8_t)(fans[fan].fanRPM>>8);
	
	buf[2+a]=(uint8_t)fans[fan].fanCur;
	buf[3+a]=(uint8_t)(fans[fan].fanCur>>8);

	buf[4+a]=fans[fan].pwmval;

	buf[5+a]=0;
	if (fans[fan].enabled) buf[5+a]|=1<<7;
	if (fans[fan].override) buf[5+a]|=1<<6;
	if (fans[fan].fault) buf[5+a]|=1<<5;
	if (fans[fan].pwmval>0) buf[5+a]|=1<<4;

	buf[6+a]=fans[fan].mintemp;
	buf[7+a]=fans[fan].maxtemp;
	
	DEBUG("Fan[");
	DEBUG(fan);
	DEBUG("] RPM[");
	DEBUG((uint16_t)fans[fan].fanRPM);
	DEBUG("] Cur[");
	DEBUG((uint16_t)fans[fan].fanCur);
	DEBUG("] PWM[");
	DEBUG(fans[fan].pwmval);
	DEBUG("] Bits[");
	DEBUG(buf[5+a]);
	DEBUG("] Min[");
	DEBUG(fans[fan].mintemp);
	DEBUG("] Max[");
	DEBUG(fans[fan].maxtemp);
	DEBUGLN("]");
}

void setFanPWM(uint8_t a,boolean override,boolean noTimeStamp) {
	if (fans[a].enabled) {
		if (fans[a].pwmval>0 && fans[a].pwmval<fans[a].minpwm) fans[a].pwmval=fans[a].minpwm;
		if (fans[a].pwmval>fans[a].maxpwm) fans[a].pwmval=fans[a].maxpwm;
		if (override) fans[a].override=true;
		analogWrite(fans[a].pwmpinnum,fans[a].pwmval);
		if (!noTimeStamp) fans[a].timestamp=millis();
	}
}

void setFanPWM(uint8_t a,boolean override) {
	setFanPWM(a,override,false);
}

void setFanDuty(uint8_t a) {
	fans[a].pwmval=i2cBuffer[1];
	setFanPWM(a,true);
}

void enableFan(uint8_t a) {
	if (!fans[a].enabled) {
		fans[a].enabled=1;
		setFanPWM(a,false);
	}
}

void disableFan(uint8_t a) {
	 analogWrite(fans[a].pwmpinnum,0);
	 fans[a].enabled=0;
}

void sendI2CStatus() 
{
	DEBUGLN("I2C Status called");
	DEBUG("status :");
	DEBUGLN(status);
	if (status>0) {
		if (status==1) {
			DEBUGLN("Sending quick Status");
			quickStatus(buf);
			Wire.write(buf,11);
		} else if (status==2) {
			DEBUGLN("Sending Full Status");
			
			quickStatus(buf);
			for (int a=0;a<4;a++) {
				fanStatus(buf,a,false);
			}
			Wire.write(buf,43);
			for (int a=0;a<43;a++)
			{
				 DEBUG(buf[a]);
				 DEBUG(" ");
			}
			DEBUGLN("");
		} else if (status>2 && status<7) {
			DEBUG("Sending Fan[");
			DEBUG(status-3);
			DEBUGLN("] Status");
			fanStatus(buf,status-3,true);
			Wire.write(buf,8);
		}
		status=0;
	} else {
		Wire.write(0);
	}
}

void processI2CEvent(int numEvents)
{
		DEBUG("I2C Write fired :");
		DEBUG(numEvents);
		DEBUG(":");
		DEBUG(Wire.available());
		DEBUG(" > ");
		i2cDataLen=0;
		while(Wire.available()) // loop through all but the last
		{
			if (i2cDataLen<16) {
				i2cBuffer[i2cDataLen]=Wire.read();
				DEBUG(i2cBuffer[i2cDataLen]);
				i2cDataLen++;
				DEBUG(" ");
			} else {
				Wire.read();
				DEBUGLN("I2C Overflow");
			}
		}
		DEBUGLN("< I2C Event Done");
	
}

void enableAllFans(){
	for (int a=0;a<4;a++) {
		enableFan(a);
	}
}

void setAllOff(){
	for (int a=0;a<4;a++) {
		fans[a].pwmval=0;
		setFanPWM(a,true);
	}
}

void processRecievedData()
{
	if (i2cDataLen>0) {
		int cmd = i2cBuffer[0]; // receive byte as a character
		DEBUG("CMD :");
		DEBUGLN(cmd);
		switch(cmd){
			case CMDFULLSTATUS:
			status=2;
			break;
			
			case CMDQUICKSTATUS:
			status=1;
			break;

			case CMDFANSTATUS1:
			status=3;
			break;

			case CMDFANSTATUS2:
			status=4;
			break;

			case CMDFANSTATUS3:
			status=5;
			break;

			case CMDFANSTATUS4:
			status=6;
			break;
			
			case CMDENABLESERIAL:
			  serialenabled=TRUE;
			  SETUPSERIAL;
			  DEBUGLN("Serial Enabled");
			break;

			case CMDENABLE:
			  enableAllFans();
			break;

			case CMDDISABLE:
			for (int a=0;a<4;a++) {
				disableFan(a);
			}
			break;

			case CMDENABLEFAN1:
			enableFan(0);
			break;

			case CMDENABLEFAN2:
			enableFan(1);
			break;

			case CMDENABLEFAN3:
			enableFan(2);
			break;

			case CMDENABLEFAN4:
			enableFan(3);
			break;

			case CMDDISABLEFAN1:
			disableFan(0);
			break;

			case CMDDISABLEFAN2:
			disableFan(1);
			break;

			case CMDDISABLEFAN3:
			disableFan(2);
			break;

			case CMDDISABLEFAN4:
			disableFan(3);
			break;

			case CMDSETALLON:
			for (int a=0;a<4;a++) {
				fans[a].pwmval=fans[a].maxpwm;
				setFanPWM(a,true);
			}
			break;
			
			case CMDSETALLOFF:
			  setAllOff();
			break;
			
			case CMDSETMINDUTY1:
			if (i2cDataLen==2) fans[0].minpwm=i2cBuffer[1];
			break;
			case CMDSETMINDUTY2:
			if (i2cDataLen==2) fans[1].minpwm=i2cBuffer[1];
			break;
			case CMDSETMINDUTY3:
			if (i2cDataLen==2) fans[2].minpwm=i2cBuffer[1];
			break;
			case CMDSETMINDUTY4:
			if (i2cDataLen==2) fans[3].minpwm=i2cBuffer[1];
			break;
			
			case CMDSETMAXDUTY1:
			if (i2cDataLen==2) fans[0].maxpwm=i2cBuffer[1];
			break;
			case CMDSETMAXDUTY2:
			if (i2cDataLen==2) fans[1].maxpwm=i2cBuffer[1];
			break;
			case CMDSETMAXDUTY3:
			if (i2cDataLen==2) fans[2].maxpwm=i2cBuffer[1];
			break;
			case CMDSETMAXDUTY4:
			if (i2cDataLen==2) fans[3].maxpwm=i2cBuffer[1];
			break;

			case CMDSETDUTY1:
			if (i2cDataLen==2) setFanDuty(0);
			break;

			case CMDSETDUTY2:
			if (i2cDataLen==2) setFanDuty(1);
			break;

			case CMDSETDUTY3:
			if (i2cDataLen==2) setFanDuty(2);
			break;

			case CMDSETDUTY4:
			if (i2cDataLen==2) setFanDuty(3);
			break;

			case CMDSETMINTEMP:
			if (i2cDataLen==2)
			for (int a=0;a<4;a++) {
				fans[a].mintemp=i2cBuffer[1];
			}
			break;

			case CMDSETMAXTEMP:
			if (i2cDataLen==2)
			for (int a=0;a<4;a++) {
				fans[a].maxtemp=i2cBuffer[1];
			}
			break;

			case CMDSETFUZZY:
			if (i2cDataLen==2) fuzzyBand=i2cBuffer[1];
			break;

			case CMDSETSTEPS:
			if (i2cDataLen==2) fanSteps=i2cBuffer[1];
			break;

			case CMDSETMINTEMP1:
			if (i2cDataLen==2) fans[0].mintemp=i2cBuffer[1];
			break;

			case CMDSETMINTEMP2:
			if (i2cDataLen==2) fans[1].mintemp=i2cBuffer[1];
			break;

			case CMDSETMINTEMP3:
			if (i2cDataLen==2) fans[2].mintemp=i2cBuffer[1];
			break;

			case CMDSETMINTEMP4:
			if (i2cDataLen==2) fans[3].mintemp=i2cBuffer[1];
			break;

			case CMDSETMAXTEMP1:
			if (i2cDataLen==2) fans[0].maxtemp=i2cBuffer[1];
			break;

			case CMDSETMAXTEMP2:
			if (i2cDataLen==2) fans[1].maxtemp=i2cBuffer[1];
			break;

			case CMDSETMAXTEMP3:
			if (i2cDataLen==2) fans[2].maxtemp=i2cBuffer[1];
			break;

			case CMDSETMAXTEMP4:
			if (i2cDataLen==2) fans[3].maxtemp=i2cBuffer[1];
			break;

			case CMDSETAUTO1:
			fans[0].override=0;
			break;
			case CMDSETAUTO2:
			fans[1].override=0;
			break;
			case CMDSETAUTO3:
			fans[2].override=0;
			break;
			case CMDSETAUTO4:
			fans[3].override=0;
			break;
			
			case CMDLEARNFAN:
			learnFans=true;
			break;
			
			case CMDSETRPMBAND:
			if (i2cDataLen==3) {
				rpmBand=i2cBuffer[1];
				rpmBand|=(unsigned long)i2cBuffer[2]<<8;
			}
			break;
			
			case CMDSETCURBAND:
			if (i2cDataLen==3) {
				curBand=i2cBuffer[1];
				curBand|=(unsigned long)i2cBuffer[2]<<8;
			}
			break;
			
			case CMDSETFAULTON:
				raiseAlarms=true;
			break;
			
			case CMDSETFAULTOFF:
				raiseAlarms=false;
			break;

			case CMDSETADDRESS:
			if (i2cDataLen==5) {
				if (i2cBuffer[2]=='S' && i2cBuffer[3]=='e' && i2cBuffer[4]=='t') {
					i2cAddress=i2cBuffer[1];
				}
			}
			break;

			case CMDFACTORYRESET:
			if (i2cDataLen==4) {
				if (i2cBuffer[1]=='Y' && i2cBuffer[2]=='e' && i2cBuffer[3]=='s') {
					defaultEeprom();
				}
			}
			break;
			case CMDCOMITCHANGES:
			if (i2cDataLen==4) {
					if (i2cBuffer[1]=='S' && i2cBuffer[2]=='e' && i2cBuffer[3]=='t') {
					storeEeprom();
				}
			}
			break;
			case CMDHARDRESET:
			pollWDT=false;
			break;
			default:
			break;
		}
		i2cDataLen=0;
	}
	
}

float thermistorToC(int RawADC,float B,float T0,float R0,float R_Balance)
{
	float R,T;
	R=R_Balance*(1024.0f/float(RawADC)-1);
	T=1.0f/(1.0f/T0+(1.0f/B)*log(R/R0));
	//T=9.0f*(T-273.15f)/5.0f+32.0f; Farenh
	T-=273.15f;
	return T;
};

void readVcc() {
	long result;
	// Read 1.1V reference against AVcc
	ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
	delay(2); // Wait for Vref to settle
	ADCSRA |= _BV(ADSC); // Convert
	while (bit_is_set(ADCSRA,ADSC));
	result = ADCL;
	result |= ADCH<<8;
	result = 1126400L / result; // Back-calculate AVcc in mV
	cpuVoltage=(float)result/1000.00;
}

void resetCur(){
	cur_total=0;
	cur_index=0;
	cur_filled=0;
	for (int a=0;a<50;a++){
		cur_fanAvg[a]=0;
	}
}

void getFanCurrent(byte fan) {
	cur_total=cur_total-cur_fanAvg[cur_index];
	//cur_fanAvg[cur_index]=(unsigned long)((float)analogRead(fans[0].curpinnum)*255)*cpuVoltage/123.457;
	//This should really be the Fan supply voltage not CPUVoltage.
	cur_fanAvg[cur_index]=(unsigned long)((float)(analogRead(fans[0].curpinnum)*cpuVoltage)/1.5345f);
	cur_total=cur_total+cur_fanAvg[cur_index];
	if (cur_filled<50) cur_filled++;
	cur_index++;
	if (cur_index>=50) cur_index=0;
	fans[fan].fanCur=cur_total/cur_filled;
}

void setFanSpeed(byte fan) {
	if (!fans[fan].override) {
		if (oneWireTempInC<=(fans[fan].mintemp-fuzzyBand)) {
			fans[fan].pwmval=0;
			analogWrite(fans[fan].pwmpinnum,fans[fan].pwmval);
			}else if (oneWireTempInC>=(fans[fan].maxtemp)) {
			fans[fan].pwmval=fans[fan].maxpwm;
			analogWrite(fans[fan].pwmpinnum,fans[fan].pwmval);
			}else if (oneWireTempInC>=(fans[fan].mintemp+fuzzyBand)) {
			fans[fan].pwmval=map((255/fanSteps)*map(oneWireTempInC,fans[fan].mintemp,fans[fan].maxtemp,1,fanSteps),1,255,fans[fan].minpwm,fans[fan].maxpwm);
			analogWrite(fans[fan].pwmpinnum,fans[fan].pwmval);
		}
	}
}

void getConversions(){
	oneWireTempInC=onewireTemp.getTempCByIndex(0)-ETEMPOFFSET;
	internalTemp=thermistorToC(analogRead(ONBOARDTEMP),3930,298.15f,10000.0f,10000.0f)-ITEMPOFFSET;
	if (oneWireTempInC<-100) {
		oneWireTempInC=internalTemp;
		noOneWire=true;
	} else noOneWire=false;
	onewireTemp.requestTemperatures();
	conversionWaitTime=millis();
	supplyVoltage=(((analogRead(FANSUPVOLT)*cpuVoltage)/1024.0)/(SUPPLYBOTR/(SUPPLYBOTR+SUPPLYTOPR)));
}

void calcFanRPM(uint8_t a) {
	uint8_t oldpwm=fans[a].pwmval;
	fans[a].pwmval=255;
	setFanPWM(a,false,true);
	boolean timedOut=false;
	fans[a].fantoggleTime=millis();
	fans[a].fantoggle=digitalRead(fans[a].senpinnum);
	while(fans[a].fantoggle==digitalRead(fans[a].senpinnum) && !timedOut) timedOut=fans[a].fantoggleTime+100<millis();
	if (timedOut) {
		fans[a].fanRPM=0;
	} else {
		fans[a].fantoggle=!fans[a].fantoggle;
		fans[a].fantoggleTime=millis();
		fans[a].fanCnt=0;
		while (fans[a].fanCnt<4 && millis()<fans[a].fantoggleTime+100) {
			if (digitalRead(fans[a].senpinnum)!=fans[a].fantoggle) {
				fans[a].fantoggle=!fans[a].fantoggle;
				fans[a].fanCnt++;
			}
		} 
		fans[a].fanRPM=(1000/((float)(millis()-fans[a].fantoggleTime)/2.0f))*60;
	}
	fans[a].pwmval=oldpwm;
	setFanPWM(a,false,true);
}

void toggleWDT(){
	wdtToggle=!wdtToggle;
	digitalWrite(WDI,wdtToggle);
}

void doLearnFans() {
	enableAllFans();
	setAllOff();
	for (int a=0;a<4;a++) {
		resetCur();
		for (int b=0;b<2;b++) {
			toggleWDT();
			if (b==0) {
				fans[a].pwmval=255;
				DEBUGLN("MAX FAN");
				} else {
				fans[a].pwmval=1;
				DEBUGLN("MIN FAN");
			}
			setFanPWM(a,false);
			long timepassed=0;
			long oldtime=0;
			long starttime=millis();
			fans[a].fanCnt=0;
			fans[a].fantoggleTime=millis();
			while(timepassed<5000){
				timepassed=millis()-starttime;
				if (timepassed-oldtime>100) {
					getFanCurrent(a);
					oldtime=timepassed;
				}
				calcFanRPM(a);
			}
			if (b==0) fans[a].maxRPM=fans[a].fanRPM;
			else {
				if (fans[a].fanRPM>fans[a].maxRPM) fans[a].minRPM=fans[a].maxRPM;
				else fans[a].minRPM=fans[a].fanRPM;
			}
		}
		
		if (fans[a].maxRPM==0) fans[a].enabled=false;
		
		fans[a].maxCurrent=fans[a].fanCur;
		fans[a].pwmval=0;
		setFanPWM(a,false);
		
		#ifdef SOFTDEBUGENABLED
		DEBUG("FAN Status[");
		DEBUG(a);
		DEBUG("]:");
		if (fans[a].enabled) DEBUGLN("Enabled");
		else DEBUGLN("Disabled");
		#endif
		DEBUG("FAN MinRpm[");
		DEBUG(a);
		DEBUG("]:");
		DEBUGLN(fans[a].minRPM);
		DEBUG("FAN MaxRpm[");
		DEBUG(a);
		DEBUG("]:");
		DEBUGLN(fans[a].maxRPM);
		DEBUG("FAN Current[");
		DEBUG(a);
		DEBUG("]:");
		DEBUGLN(fans[a].maxCurrent);
	}
	resetCur();
	toggleWDT();
	learnFans=false;
}

void checkForFaults(){
	
	uint32_t totcur;
	for (int a=0;a<4;a++) {
		boolean faultFound=false;
		if (fans[a].enabled) {
			if (fans[a].pwmval>0 && (fans[a].timestamp+1000)<millis()) {
				if (fans[a].fanRPM+rpmBand<fans[a].minRPM) {
					faultFound=true;
					fans[a].faultreason|=1;
				}
				if (fans[a].fanRPM>fans[a].maxRPM+rpmBand) {
					faultFound=true;
					fans[a].faultreason|=2;
				}
			}
			totcur+=fans[a].maxCurrent;
			if (faultFound) {
				systemFault=true;
				fans[a].fault=true;
			} else {
				fans[a].fault=false;
			}
			if (fans[a].pwmval>0) fansOn=true;
		}
	}
	if (fans[0].fanCur>totcur) {
		systemFault=true;
		fans[0].fault=true;
		fans[0].faultreason|=4;
	}
	#ifdef SOFTDEBUGENABLED
	if (millis() - serialTime >2000) {
		if (raiseAlarms) {
			if (systemFault) {
				DEBUGLN("System Fault");
				systemFault=false;
				for (int a=0;a<4;a++) {
					DEBUGLN(fans[a].faultreason);
					fans[a].faultreason=0;
				}
			}
		} else {
			if (fansOn) {				
				DEBUGLN("Fan On");
				fansOn=false;
			}
		}
	}
	#else
	if (raiseAlarms) {
		digitalWrite(FANON,systemFault);
		systemFault=false;
	} else {
		digitalWrite(FANON,fansOn);
		fansOn=false;
	}
	#endif
}

void loop() {
	if (learnFans) {
		doLearnFans();
	}

	
	for (int a=0;a<4;a++) {
		if (fans[a].enabled) {
			if (fans[a].pwmval<fans[a].minpwm) {
				fans[a].fanRPM=0;
			} else {
				calcFanRPM(a);
			}
		} else {
			fans[a].fanRPM=0;
		}
	}
	
	if (millis() - conversionWaitTime > conversionPollPeriod) {
		
		getConversions();
		getFanCurrent(0);
		for (int a=0;a<4;a++) {
			setFanSpeed(a);
		}
	}
	
	if ((millis() - serialTime > 500) && pollWDT) {
		toggleWDT();
	}
	
	processRecievedData();
	
	checkForFaults();
	
#ifdef VERBOSEOUT
	if (millis() - serialTime >2000) {
		DEBUG(loopcount);
		DEBUG("- I:");
		DEBUG(internalTemp);
		DEBUG(" E:");
		DEBUG(oneWireTempInC);
		if (noOneWire) DEBUG("*");
		DEBUG(" V:");
		DEBUG(supplyVoltage);
		DEBUG(" C:");
		DEBUG(cpuVoltage);
		DEBUG(" R:[");
		DEBUG(fans[0].fanRPM);
		DEBUG("][");
		DEBUG(fans[1].fanRPM);
		DEBUG("][");
		DEBUG(fans[2].fanRPM);
		DEBUG("][");
		DEBUG(fans[3].fanRPM);
		DEBUG("] P:[");
		DEBUG(fans[0].pwmval);
		DEBUG("][");
		DEBUG(fans[1].pwmval);
		DEBUG("][");
		DEBUG(fans[2].pwmval);
		DEBUG("][");
		DEBUG(fans[3].pwmval);
		DEBUG("] I:[");
		DEBUG(fans[0].fanCur);
		DEBUG("][");
		DEBUG(fans[1].fanCur);
		DEBUG("][");
		DEBUG(fans[2].fanCur);
		DEBUG("][");
		DEBUG(fans[3].fanCur);
		DEBUG("] -T:[");
		DEBUG(fans[0].mintemp);
		DEBUG("][");
		DEBUG(fans[1].mintemp);
		DEBUG("][");
		DEBUG(fans[2].mintemp);
		DEBUG("][");
		DEBUG(fans[3].mintemp);
		DEBUG("] +T:[");
		DEBUG(fans[0].maxtemp);
		DEBUG("][");
		DEBUG(fans[1].maxtemp);
		DEBUG("][");
		DEBUG(fans[2].maxtemp);
		DEBUG("][");
		DEBUG(fans[3].maxtemp);
		DEBUGLN("]");
		serialTime=millis();
		loopcount++;		
	}
#endif	
	
	
}
