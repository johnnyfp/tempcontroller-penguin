/* 
	Editor: http://www.visualmicro.com
	        visual micro and the arduino ide ignore this code during compilation. this code is automatically maintained by visualmicro, manual changes to this file will be overwritten
	        the contents of the Visual Micro sketch sub folder can be deleted prior to publishing a project
	        all non-arduino files created by visual micro and all visual studio project or solution files can be freely deleted and are not required to compile a sketch (do not delete your own code!).
	        note: debugger breakpoints are stored in '.sln' or '.asln' files, knowledge of last uploaded breakpoints is stored in the upload.vmps.xml file. Both files are required to continue a previous debug session without needing to compile and upload again
	
	Hardware: Arduino Pro or Pro Mini (5V, 16 MHz) w/ ATmega328, Platform=avr, Package=arduino
*/

#define __AVR_ATmega328p__
#define __AVR_ATmega328P__
#define ARDUINO 106
#define ARDUINO_MAIN
#define F_CPU 16000000L
#define __AVR__
extern "C" void __cxa_pure_virtual() {;}

//
void storeEeprom();
void readEeprom();
void defaultEeprom();
void quickStatus(uint8_t *buf);
void fanStatus(uint8_t *buf,uint8_t fan,boolean fs);
void setFanPWM(uint8_t a,boolean override,boolean noTimeStamp);
void setFanPWM(uint8_t a,boolean override);
void setFanDuty(uint8_t a);
void enableFan(uint8_t a);
void disableFan(uint8_t a);
void sendI2CStatus();
void processI2CEvent(int numEvents);
void enableAllFans();
void setAllOff();
void processRecievedData();
float thermistorToC(int RawADC,float B,float T0,float R0,float R_Balance);
void readVcc();
void resetCur();
void getFanCurrent(byte fan);
void setFanSpeed(byte fan);
void getConversions();
void calcFanRPM(uint8_t a);
void toggleWDT();
void doLearnFans();
void checkForFaults();
//

#include "D:\Programming\arduino-1.0.6\hardware\arduino\variants\standard\pins_arduino.h" 
#include "D:\Programming\arduino-1.0.6\hardware\arduino\cores\arduino\arduino.h"
#include "D:\Programming\Projects\TempController - Penguin\software\TempFanControllerFirmware\TempFanControllerFirmware.ino"
