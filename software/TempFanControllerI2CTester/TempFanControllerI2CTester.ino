#define CMDFULLSTATUS   1  //Returns a full status of Controller
#define CMDQUICKSTATUS  2  //Returns a quick status
#define CMDFANSTATUS1	3  //Current status of Fan 1
#define CMDFANSTATUS2	4  //Current status of Fan 2
#define CMDFANSTATUS3	5  //Current status of Fan 3
#define CMDFANSTATUS4	6  //Current status of Fan 4
#define CMDENABLE		7  //Enable all fans
#define CMDDISABLE		8  //Disable all fans
#define CMDENABLEFAN1	9  //Enable fan 1
#define CMDENABLEFAN2	10 //Enable fan 2
#define CMDENABLEFAN3	11 //Enable fan 3
#define CMDENABLEFAN4	12 //Enable fan 4
#define CMDDISABLEFAN1	13 //Disable fan 1
#define CMDDISABLEFAN2	14 //Disable fan 2
#define CMDDISABLEFAN3	15 //Disable fan 3
#define CMDDISABLEFAN4	16 //Disable fan 4
#define CMDSETALLON		17 //Sets Duty on all fans to 100%
#define CMDSETALLOFF	18 //Sets Duty on all fans to 0%
#define CMDSETDUTY1		19 //Sets Duty on fan 1
#define CMDSETDUTY2		20 //Sets Duty on fan 1
#define CMDSETDUTY3		21 //Sets Duty on fan 1
#define CMDSETDUTY4		22 //Sets Duty on fan 1
#define CMDSETMINTEMP	23 //Sets the Off Temperature for all fans
#define CMDSETMAXTEMP	24 //Sets the Max Duty Temperature for all fans
#define CMDSETFUZZY		25 //Sets the Switchover band to stop pulsing
#define CMDSETSTEPS		26 //Sets the number of duty steps between off and max (0 - Max temp fan comes on, Off temp fan switched off)
#define CMDSETMINTEMP1  27 //Sets the Off Temperature for fan 1
#define CMDSETMINTEMP2  28 //Sets the Off Temperature for fan 2
#define CMDSETMINTEMP3  29 //Sets the Off Temperature for fan 3
#define CMDSETMINTEMP4  30 //Sets the Off Temperature for fan 4
#define CMDSETMAXTEMP1	31 //Sets the Max Temperature for fan 1
#define CMDSETMAXTEMP2	32 //Sets the Max Temperature for fan 2
#define CMDSETMAXTEMP3	33 //Sets the Max Temperature for fan 3
#define CMDSETMAXTEMP4	34 //Sets the Max Temperature for fan 4
#define CMDLEARNFAN		35 //Learns the fans max speed and current usage.
#define CMDSETADDRESS	36 //Defines the I2C address to use (44 is the default address) Must be followed with 83 101 116 (Set).
#define CMDFACTORYRESET 37 //Clears the EEPROM Must be followed with 89 101 115 (Yes)
#define CMDCOMITCHANGES 38 //Commits the current settings to EEPROM. Must be followed with 83 101 116 (Set)
#define CMDHARDRESET	39 //Performs a hard reset to reload from EEPROM. Must have WDT installed.
#define CMDSETMINDUTY1	40 //Sets the Minimum duty allowed for fan 1
#define CMDSETMINDUTY2	41 //Sets the Minimum duty allowed for fan 2
#define CMDSETMINDUTY3	42 //Sets the Minimum duty allowed for fan 3
#define CMDSETMINDUTY4	43 //Sets the Minimum duty allowed for fan 4
#define CMDSETMAXDUTY1	44 //Sets the Maximum duty allowed for fan 1
#define CMDSETMAXDUTY2	45 //Sets the Maximum duty allowed for fan 2
#define CMDSETMAXDUTY3	46 //Sets the Maximum duty allowed for fan 3
#define CMDSETMAXDUTY4	47 //Sets the Maximum duty allowed for fan 4
#define CMDSETAUTO1	48 //Sets a fan to be temperature controlled, switching off the override.
#define CMDSETAUTO2	49 //Sets a fan to be temperature controlled, switching off the override.
#define CMDSETAUTO3	50 //Sets a fan to be temperature controlled, switching off the override.
#define CMDSETAUTO4	51 //Sets a fan to be temperature controlled, switching off the override.
#define CMDSETRPMBAND   52 //Sets the Window that the RPM is allowed to be in.
#define CMDSETCURBAND	53 //Sets the Window that the Current is allowed to be in.
#define CMDSETFAULTON   54 //Sets FANON pin to indicate if there is a fan fault
#define CMDSETFAULTOFF  55 //Sets FANON pin to indicate that fans are on or off
#define CMDENABLESERIAL 56 //Enables Serial Debugging on FANON

#include <Wire.h>

String cmdRead;
String value1;
String value2;
uint8_t wireData[16];
uint8_t slaveAddress=44;
boolean cmdFound,val1Found,val2Found,execute=false;

void setup()
{
  Wire.begin();        // join i2c bus (address optional for master)
  Serial.begin(115200);  // start serial for output
}

void sendWire(uint8_t cmd,uint8_t data[],uint8_t len) {
  Serial.print("Sending [");
  Serial.print(cmd);
  Serial.print("]");
  if (len>0) Serial.print(" with data");
  Wire.beginTransmission(slaveAddress);
  Wire.write(cmd);
  for (uint8_t a=0;a<len;a++){
    Serial.print("[");
    Serial.print(data[a]);
    Serial.print("]");
    Wire.write(data[a]);
  }
  Wire.endTransmission();
  Serial.println();
}

void outBool(uint8_t a) {
  for (uint8_t b=0;b<4;b++) {
    if (a&(1<<b)) Serial.print("1");
    else Serial.print("0");
    Serial.print(" ");
  }
  Serial.println();
}

void fullStatus(){
  Serial.println("Reading Full Status");
  Wire.beginTransmission(slaveAddress);
  Wire.write(CMDFULLSTATUS);
  Wire.endTransmission();
  delay(100);
  uint8_t buf[43];
  uint8_t a=0;
  uint8_t cnt=0;
  Wire.requestFrom(slaveAddress,(uint8_t)43);
  delay(2);
  while(Wire.available())
  {
    uint8_t r=Wire.read();
    buf[a++]=r;
    cnt++;
  }
  Serial.print("Read ");
  Serial.println(cnt);
  for (a=0;a<43;a++) {
    Serial.print(buf[a]);
    Serial.print(" ");
  }
  printStatus(buf);
  for (a=0;a<4;a++) {
    printFanStatus(buf,a);
  }
}

void printFanStatus(uint8_t* buf,int8_t fan){
  Serial.print("Fan status");
  uint8_t a;
  if (fan==-1) a=0;
  else {
    a=11+fan*8;
    Serial.print(" [");
    Serial.print(fan+1);
    Serial.print("]");
  }
  Serial.println();
  Serial.print("Fan RPM :");
  Serial.println(buf[0+a]|((uint16_t)buf[1+a])<<8);
  Serial.print("Fan Current :");
  Serial.println(buf[2+a]|((uint16_t)buf[3+a])<<8);
  Serial.print("Fan PWM :");
  Serial.println(buf[4+a]);
  Serial.println("Fan Status : A F O E");
  Serial.print("             ");
  outBool(buf[5+a]>>4);
  Serial.print("Min Temp :");
  Serial.println(buf[6+a]);
  Serial.print("Max Temp :");
  Serial.println(buf[7+a]);
  Serial.println();
}
  
  
void printStatus(uint8_t* buf){
  Serial.print("Internal Temp : ");
  Serial.println(buf[0]);
  Serial.print("OneWire Temp : ");
  Serial.println(buf[1]);
  int cur=buf[2];
  cur|=buf[3]<<8;
  Serial.print("Current : ");
  Serial.println(cur);
  Serial.println("    Fans : 1 2 3 4");
  Serial.print(" Enabled : ");
  outBool(buf[6]>>4);
  Serial.print("  Active : ");
  outBool(buf[6]);
  Serial.print("   Fault : ");
  outBool(buf[5]);
  Serial.print("Override : ");
  outBool(buf[4]);
  
  float sv=(float)buf[7]+(float)buf[8]/100.0;
  float cv=(float)buf[9]+(float)buf[10]/100.0;
  Serial.print("Supply Voltage :");
  Serial.println(sv);
  Serial.print("CPU Voltage :");
  Serial.println(cv);
}

void fanStatus(uint8_t cmd){
  Serial.println("Reading Fan Status");
  Wire.beginTransmission(slaveAddress);
  Wire.write(cmd);
  Wire.endTransmission();
  delay(100);
  uint8_t buf[8];
  uint8_t a=0;
  Wire.requestFrom(slaveAddress,(uint8_t)8);
  while(Wire.available())
  {
    buf[a++]= Wire.read();
  }
  printFanStatus(buf,-1);
}

void readStatus() {
  Serial.println("Reading Status");
  Wire.beginTransmission(slaveAddress);
  Wire.write(CMDQUICKSTATUS);
  Wire.endTransmission();
  delay(100);
  uint8_t buf[11];
  uint8_t a=0;
  Wire.requestFrom(slaveAddress,(uint8_t)11);
  while(Wire.available())
  {
    buf[a++]= Wire.read();
  }
  Serial.print("Internal Temp : ");
  Serial.println(buf[0]);
  Serial.print("OneWire Temp : ");
  Serial.println(buf[1]);
  int cur=buf[2];
  cur|=buf[3]<<8;
  Serial.print("Current : ");
  Serial.println(cur);
  Serial.println("    Fans : 1 2 3 4");
  Serial.print(" Enabled : ");
  outBool(buf[6]>>4);
  Serial.print("  Active : ");
  outBool(buf[6]);
  Serial.print("   Fault : ");
  outBool(buf[5]);
  Serial.print("Override : ");
  outBool(buf[4]);
  
  float sv=(float)buf[7]+(float)buf[8]/100.0;
  float cv=(float)buf[9]+(float)buf[10]/100.0;
  Serial.print("Supply Voltage :");
  Serial.println(sv);
  Serial.print("CPU Voltage :");
  Serial.println(cv);
}

void processSerial()
{
   while (Serial.available() >0) {
     char c=Serial.read();
     if (c==' ') {
       if (!cmdFound) cmdFound=true;
       else if (!val1Found) val1Found=true;
       else if (!val2Found) val2Found=true;
       continue;
     }
     if (c=='\n') {
       if (val1Found) val2Found=true;
       if (cmdFound) val1Found=true;
       cmdFound=true;
       execute=true;
       break;
     }
     
     if (!cmdFound) cmdRead+=c;
     else if(!val1Found) value1+=c;
     else if(!val2Found) value2+=c;
     
   }
   
   if (execute) {
     Serial.print("CMD [");
     Serial.print(cmdRead);
     Serial.print("] Value 1 [");
     Serial.print(value1);
     Serial.print("] Value 2 [");
     Serial.print(value2);
     Serial.println("]");
     if (cmdRead.equalsIgnoreCase("HELP")) { 
       Serial.println("fullstatus");
       Serial.println("quickstatus");
       Serial.println("fanstatus {1-4}");
       Serial.println("enable");
       Serial.println("disable");
       Serial.println("enablefan {1-4}");
       Serial.println("disablefan {1-4}");
       Serial.println("setallon");
       Serial.println("setalloff");
       Serial.println("setduty {1-4} {0-255}");
       Serial.println("setmintemp {1-4} {-127 - 127}");
       Serial.println("setmaxtemp {1-4} {-127 - 127}");
       Serial.println("setfuzzy {0-255}");
       Serial.println("setsteps {0-255}");
       Serial.println("learnfan");
       Serial.println("setaddress {0-255} Set");
       Serial.println("factoryreset Yes");
       Serial.println("comitchanges Set");
       Serial.println("hardreset");
       Serial.println("setminduty {1-4} {0-255}");
       Serial.println("setmaxduty {1-4} {0-255}");
       Serial.println("setauto {1-4}");
       Serial.println("setrpmband {0-65535}");
       Serial.println("setcurband {0-65535}");
       Serial.println("setfaulton");
       Serial.println("setfaultoff");
       Serial.println("enableserial");
       Serial.println("All commands must follow a newline to send");
     }else if (cmdRead.equalsIgnoreCase("FULLSTATUS")) { 
       fullStatus();
     }else if (cmdRead.equalsIgnoreCase("QUICKSTATUS")) {
       //sendWire(CMDFULLSTATUS,wireData,0);
       readStatus();
     }else if (cmdRead.equalsIgnoreCase("FANSTATUS")) {
       uint8_t a=value1.toInt()-1;
       fanStatus(CMDFANSTATUS1+a);
     }else if (cmdRead.equalsIgnoreCase("ENABLE")) {
       sendWire(CMDENABLE,wireData,0);
     }else if (cmdRead.equalsIgnoreCase("DISABLE")) {
       sendWire(CMDDISABLE,wireData,0);
     }else if (cmdRead.equalsIgnoreCase("SETFAULTON")) {
       sendWire(CMDSETFAULTON,wireData,0);
     }else if (cmdRead.equalsIgnoreCase("SETFAULTOFF")) {
       sendWire(CMDSETFAULTOFF,wireData,0);
     }else if (cmdRead.equalsIgnoreCase("CMDENABLESERIAL")) {
       sendWire(CMDENABLESERIAL,wireData,0);
     }else if (cmdRead.equalsIgnoreCase("SETAUTO")) {
       uint8_t a=value1.toInt()-1;
       if (a>3) {
         Serial.println("Fan Number out of range [1-4]");
       } else
       sendWire(CMDSETAUTO1+a,wireData,0);
     }else if (cmdRead.equalsIgnoreCase("ENABLEFAN")) {
       uint8_t a=value1.toInt()-1;
       if (a>3) {
         Serial.println("Fan Number out of range [1-4]");
       } else
       sendWire(CMDENABLEFAN1+a,wireData,0);
     }else if (cmdRead.equalsIgnoreCase("SETAUTO")) {
       uint8_t a=value1.toInt()-1;
       if (a>3) {
         Serial.println("Fan Number out of range [1-4]");
       } else
       sendWire(CMDENABLEFAN1+a,wireData,0);
     }else if (cmdRead.equalsIgnoreCase("DISABLEFAN")) {
       uint8_t a=value1.toInt()-1;
       if (a>3) {
         Serial.println("Fan Number out of range [1-4]");
       } else
       sendWire(CMDDISABLEFAN1+a,wireData,0);
     }else if (cmdRead.equalsIgnoreCase("SETALLON")) {
       sendWire(CMDSETALLON,wireData,0);
     }else if (cmdRead.equalsIgnoreCase("SETALLOFF")) {
       sendWire(CMDSETALLOFF,wireData,0);
     }else if (cmdRead.equalsIgnoreCase("SETDUTY")) {
       uint8_t a=value1.toInt()-1;
       if (a>3) {
         Serial.println("Fan Number out of range [1-4]");
       } else {
       wireData[0]=value2.toInt();
       sendWire(CMDSETDUTY1+a,wireData,1);
       }
     }else if (cmdRead.equalsIgnoreCase("SETMINTEMP")) {
       uint8_t a=value1.toInt()-1;
       if (a>3) {
         Serial.println("Fan Number out of range [1-4]");
       } else {
       wireData[0]=value2.toInt();
       sendWire(CMDSETMINTEMP1+a,wireData,1);
       }
     }else if (cmdRead.equalsIgnoreCase("SETMAXTEMP")) {
       uint8_t a=value1.toInt()-1;
       if (a>3) {
         Serial.println("Fan Number out of range [1-4]");
       } else {
       wireData[0]=value2.toInt();
       sendWire(CMDSETMAXTEMP1+a,wireData,1);
       }
     }else if (cmdRead.equalsIgnoreCase("SETFUZZY")) {
       wireData[0]=value1.toInt();
       sendWire(CMDSETFUZZY,wireData,1);
     }else if (cmdRead.equalsIgnoreCase("SETRPMBAND")) {
       wireData[0]=(uint8_t)value1.toInt();
       wireData[1]=(uint8_t)(value1.toInt()>>8);
       sendWire(CMDSETRPMBAND,wireData,2);
     }else if (cmdRead.equalsIgnoreCase("SETCURBAND")) {
       wireData[0]=(uint8_t)value1.toInt();
       wireData[1]=(uint8_t)(value1.toInt()>>8);
       sendWire(CMDSETCURBAND,wireData,2);
     }else if (cmdRead.equalsIgnoreCase("SETSTEPS")) {
       wireData[0]=value1.toInt();
       sendWire(CMDSETSTEPS,wireData,1);
     }else if (cmdRead.equalsIgnoreCase("LEARNFAN")) {
       sendWire(CMDLEARNFAN,wireData,0);
     }else if (cmdRead.equalsIgnoreCase("ENABLESERIAL")) {
       sendWire(CMDENABLESERIAL,wireData,0);
     }else if (cmdRead.equalsIgnoreCase("SETADDRESS")) {
       wireData[0]=value1.toInt();
       uint8_t buf[3];
       value2.getBytes(buf,4);
       memcpy(wireData+1,buf,3);
       sendWire(CMDSETADDRESS,wireData,4);
       slaveAddress=value1.toInt();
     }else if (cmdRead.equalsIgnoreCase("FACTORYRESET")) {
       value1.getBytes(wireData,4);
       sendWire(CMDFACTORYRESET,wireData,3);
     }else if (cmdRead.equalsIgnoreCase("COMITCHANGES")) {
       value1.getBytes(wireData,4);
       sendWire(CMDCOMITCHANGES,wireData,3);
     }else if (cmdRead.equalsIgnoreCase("HARDRESET")) {
       sendWire(CMDHARDRESET,wireData,0);
     }else if (cmdRead.equalsIgnoreCase("SETMINDUTY")) {
       uint8_t a=value1.toInt()-1;
       if (a>3) {
         Serial.println("Fan Number out of range [1-4]");
       } else {
       wireData[0]=value2.toInt();
       sendWire(CMDSETMINDUTY1+a,wireData,1);
       }
     }else if (cmdRead.equalsIgnoreCase("SETMAXDUTY")) {
      uint8_t a=value1.toInt()-1;
      if (a>3) {
         Serial.println("Fan Number out of range [1-4]");
       } else {
      wireData[0]=value2.toInt();
      sendWire(CMDSETMAXDUTY1+a,wireData,1); 
       }
     }
     
     cmdRead="";
     value1="";
     value2="";
     execute=false;
     cmdFound=false;
     val1Found=false;
     val2Found=false;
   }
	
}

void loop()
{

  processSerial();
  return;
  Wire.requestFrom(2, 6);    // request 6 bytes from slave device #2

  while(Wire.available())    // slave may send less than requested
  { 
    char c = Wire.read(); // receive a byte as character
    Serial.print(c);         // print the character
  }

  delay(500);
}
